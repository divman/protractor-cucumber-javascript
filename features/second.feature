Feature: Running sample feature for again in another feature

    @test @high @smoke @regression
    Scenario: Login to github and verify it works from the second feature of scenario one
    Given Launch GITHUB app with "test@test.com" and "githubtest"
    When I click the Create an account
    Then I click on the github icon and navigate to home page
    And I goto Pricing page
    Then I verify the title text is "Plans for all workflows"

    @test
    Scenario: Login to github and verify it works from the second feature of scenario one
    Given Launch GITHUB app with "test@test.com" and "githubtest"
    When I click the Create an account
    Then I click on the github icon and navigate to home page
    And I goto Pricing page
    Then I verify the title text is "Plans for all workflows"

    @test @high @smoke @regression
    Scenario: Login to github and verify it works from the second feature of scenario one
    Given Launch GITHUB app with "test@test.com" and "githubtest"
    When I click the Create an account
    Then I click on the github icon and navigate to home page
    And I goto Pricing page
    Then I verify the title text is "Plans for all workflows - fail"

