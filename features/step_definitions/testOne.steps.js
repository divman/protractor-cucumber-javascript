import { Given, When, Then } from "cucumber"
import { expect } from "chai";

const stepTimeoutExpiry = {timeout: 2 * 5000}
let globalLoanNumber = ""

Given(/^Launch GITHUB app with "(.*?)" and "(.*?)"$/, stepTimeoutExpiry, async (user, pass) => {
// Given(/^Launch SATURN app with "(.*?)" and "(.*?)"$/, async (user, pass) => {
    await global.loginPage.launchApp()
    await global.loginPage.login(user, pass)
})

When (/^I click the Create an account$/, stepTimeoutExpiry, async () => {
    await global.loginPage.clickCreateAnAccount()
})

Then (/^I click on the github icon and navigate to home page$/, stepTimeoutExpiry, async () => {
    await global.loginPage.navigateHomePage()
})

Then (/^I goto Pricing page$/, stepTimeoutExpiry, async () => {
    await global.loginPage.getPricingPage()
})

Then (/^I verify the title text is "(.*?)"$/, stepTimeoutExpiry, async (titleText) => {
    await global.loginPage.getPriceTitle(titleText)
})

// sample different steps

// When(/^I search a loan by "(.*?)" as "(.*?)" in search page$/, stepTimeoutExpiry, async (searchType, loanNumber) => {
//     globalLoanNumber = loanNumber
//     await global.welcomePage.selectSearchType(searchType)
//     await global.welcomePage.enterLoanNumber(loanNumber)
//     await global.welcomePage.submitSearchBtn()
// })

// When(/^I skip the verification details on verification page$/, stepTimeoutExpiry, async () => {
//     await global.verificationPage.skipVerification()
// })

// Then(/^I must see the Saturn "(.*?)" page for this account$/, stepTimeoutExpiry, async (homeText) => {
//     // await homePage.isHomeLinkAvailable()
//     expect(await global.homePage.isHomeLinkAvailable()).to.be.true
//     expect(await global.homePage.getAccountNumberText()).to.equal(globalLoanNumber)
// })

// Then(/^I close the browser$/, stepTimeoutExpiry, async () => {
//     await browser.quit()
// })
