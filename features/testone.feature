Feature: Running sample feature

    @smoke @regression
    Scenario: Login to github and verify it works in testone feature of scenario one
    Given Launch GITHUB app with "test@test.com" and "githubtest"
    When I click the Create an account
    Then I click on the github icon and navigate to home page
    And I goto Pricing page
    Then I verify the title text is "Plans for all workflows"

    @regression @high
    Scenario: Login to github and verify it works in testone feature of scenario two
    Given Launch GITHUB app with "test@test.com" and "githubtest"
    When I click the Create an account
    Then I click on the github icon and navigate to home page
    And I goto Pricing page
    Then I verify the title text is "Plans for all workflows"

    @smoke @test
    Scenario: Login to github and verify it works in testone feature of scenario three
    Given Launch GITHUB app with "test@test.com" and "githubtest"
    When I click the Create an account
    Then I click on the github icon and navigate to home page
    And I goto Pricing page
    Then I verify the title text is "Plans for all workflows - fail"
