// sample different step approach

import { HomePageRepository } from "./elements/homePageElements";

export class HomePageFactory extends HomePageRepository {

    constructor() {
        super()
    }

    async isHomeLinkAvailable() {
        await super.waitForHomeLink()
        return await super.isHomeLinkPresentInPage()
    }

    async getAccountNumberText() {
        return await super.getAccountNumber()
    }
}