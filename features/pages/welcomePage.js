// sample different step approach

import { WelcomePageRepository } from "./elements/welcomePageElements";

export class WelcomePageFactory extends WelcomePageRepository {

    constructor() {
        super()
    }

    async selectSearchType(searchType) {
        browser.ignoreSynchronization = false;
        await super.waitForSelectSearchType()
        if (searchType === "Loan Number") {
            await super.selectSearchByLoan()
        }
    }

    async enterLoanNumber(loanNumber) {
        await super.setLoanNumber(loanNumber)
    }

    async submitSearchBtn() {
        await super.clickSearchAccountBtn()
    }
}