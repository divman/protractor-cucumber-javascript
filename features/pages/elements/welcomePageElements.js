// sample different step approach

export class WelcomePageRepository {
    constructor() {
        this.EC = protractor.ExpectedConditions
        this.elementSearchContext = element(by.id('searchType'))
        this.elementLoanNumber = element(by.linkText('Loan Number'))
        this.elementinputLoanIDText = element(by.id('loanID'))
        this.elementSearchAccountBtn = element(by.id('searchAccount'))
    }

    async waitForSelectSearchType() {
        const visibilityOfSearchContext = this.EC.visibilityOf(this.elementSearchContext)
        await browser.wait(visibilityOfSearchContext, 10000)
        await this.elementSearchContext.click()
    }

    async selectSearchByLoan() {
        await this.elementLoanNumber.click()
    }

    async setLoanNumber(loanNumber) {
        await this.elementinputLoanIDText.sendKeys(loanNumber)
    }

    async clickSearchAccountBtn() {
        await this.elementSearchAccountBtn.click()
    }
}