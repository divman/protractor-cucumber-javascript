// sample different step approach

export class VerificationPageRepository {
    constructor() {
        this.EC = protractor.ExpectedConditions
        this.elementSkipVerification = element(by.id('skipVerifyId'))
    }

    async waitForSkipVerificationLink() {
        const visibilityOfSkipVerification = this.EC.visibilityOf(this.elementSkipVerification)
        await browser.wait(visibilityOfSkipVerification, 10000)
    }

    async skipVerification() {
        await this.elementSkipVerification.click()
    }
}