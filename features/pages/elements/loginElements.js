// sample different step approach

export class LoginPageRepository {
    constructor() {
        this.EC = protractor.ExpectedConditions
        this.userName = element(by.id('username'));
        this.passWord = element(by.id('password'));
        this.signOn = element(by.xpath('.//a[contains(@title,"Sign In")]'));
    }

    async waitForUserName() {
        const visibilityOfUserName = this.EC.visibilityOf(this.userName)
        await browser.wait(visibilityOfUserName, 10000)
    }

    async setUserName(user) {
        await this.userName.sendKeys(user)
    }

    async setPassWord(pass) {
        await this.passWord.sendKeys(pass)
    }

    async clickSignOn() {
        await this.signOn.click()
    }
}