// sample different step approach

export class HomePageRepository {
    constructor() {
        this.EC = protractor.ExpectedConditions
        this.elementHomeLink = element(by.id('home'))
        this.elementAccountNumber = element(by.css('span[ng-bind="account"]'))
    }

    async waitForHomeLink() {
        const visibilityOfHomeLink = this.EC.visibilityOf(this.elementHomeLink)
        await browser.wait(visibilityOfHomeLink, 10000)
    }

    async isHomeLinkPresentInPage() {
        return await this.elementHomeLink.isDisplayed()
    }

    async getAccountNumber() {
        return await this.elementAccountNumber.getText()
    }
}