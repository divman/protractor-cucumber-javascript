// sample different step approach

import { VerificationPageRepository } from "./elements/verificationPageElements";

export class VerificationPageFactory extends VerificationPageRepository {

    constructor() {
        super()
    }

    async skipVerification() {
        await super.skipVerification()
    }
}