import { LoginPageRepository } from "./elements/loginElements";
import { logger } from "../../support/evenstLogger";
import { expect } from "chai";

// export class LoginPageFactory extends LoginPageRepository {
export class LoginPageFactory {

    constructor() {
        // super()
        this.EC = protractor.ExpectedConditions
        this.userName = element(by.id('login_field'));
        this.passWord = element(by.id('password'));
        this.signIn = element(by.xpath('.//a[contains(text(),"Sign in")]'));
        this.signInSubmit = element(by.css('input[value="Sign in"]'))
        this.errorMsg = element(by.css('div[class="container"]'))
        this.createAccount = element(by.xpath('.//a[contains(text(),"Create an account")]'));
        this.githubImage = element(by.tagName("svg"))
        this.pricing = element(by.xpath('.//a[contains(text(),"Pricing")]'));
        this.priceTitleText = element(by.className('h00-mktg'))
    }

    async launchApp() {
        // to inform the protractor that it is angular or non-angular page.
        // set the ignoreSynchronization as true, if it is non-angular weg page or web application.
        browser.ignoreSynchronization = true
        // const session = await browser.driver.getSession()
        // logger.info("Session id is : " + session.getId())
        browser.manage().window().maximize()
        await browser.driver.get('https://www.github.com');
        logger.info("Github application launched.")

    }

    async login(user, pass) {
        const visibilityOfSignIn = this.EC.visibilityOf(this.signIn)
        await browser.wait(visibilityOfSignIn, 10000)
        await this.signIn.click()
        const visibilityOfUserName = this.EC.visibilityOf(this.userName)
        await browser.wait(visibilityOfUserName, 10000)
        await this.userName.sendKeys(user)
        await this.passWord.sendKeys(pass)
        await this.signInSubmit.click()
        // await this.passWord.sendKeys(protractor.Key.ENTER)

        const visibilityOfErrorMessage = this.EC.visibilityOf(this.errorMsg)
        await browser.wait(visibilityOfErrorMessage, 10000)
        const errorMessage = await this.errorMsg.getText()
        console.log(`errorMessage is : ${errorMessage}`)
        expect(errorMessage).to.equal("Incorrect username or password.")

        // await super.waitForUserName()
        // await super.setUserName(user)
        // await super.setPassWord(pass)
        // await super.clickSignOn()
        // await browser.sleep(2000)
        // browser.ignoreSynchronization = false;
    }

    async clickCreateAnAccount() {
        await this.createAccount.click()
    }

    async navigateHomePage() {
        const visibilityOfgitImage = this.EC.visibilityOf(this.githubImage)
        await browser.wait(visibilityOfgitImage, 10000)
        await this.githubImage.click()
    }

    async getPricingPage() {
        const visibilityOfPricing = this.EC.visibilityOf(this.pricing)
        await browser.wait(visibilityOfPricing, 10000)
        await this.pricing.click()
    }

    async getPriceTitle(titleText) {
        const visibilityOfPriceText = this.EC.visibilityOf(this.priceTitleText)
        await browser.wait(visibilityOfPriceText, 10000)
        expect(await this.priceTitleText.getText()).to.equal(titleText)
    }
}
