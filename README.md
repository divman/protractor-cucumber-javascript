<p align="center">
<img src= "./images/framework_pictures.jpg" height=300 alt="titleImage.png"/>
</p>

<p align="center">
   <i><strong>This framework briefly describes the basic protractor-cucumber-javascript project setup.
</strong></i>
<p>

--------

## **A brief setup guide for Protractor-Cucumber-Javascript framework**

### **Features**
* Latest ES6 features.
* Hooks implemented for Before and After on scenarios.
* Page Object design pattern implementation.
* Report generation with cucumber html reporter.
* Scenarios running with tags
* Logging with log4js
* Screenshot capture for failure scenarios

### **Pre-requisites**
1. [NodeJs](https://nodejs.org/en/) (v>8.6) should be installed globally in the system.
2. Chrome or Firefox browser should be installed. In this demo chrome browser is used, but you can still use firefox.
3. Any text editor of your choice [Visual Studio Code](https://code.visualstudio.com/) / [Atom](https://atom.io/) / [Sublime Text](https://www.sublimetext.com/) can be installed.

### **Setup Scripts**

* Clone the repository into a local folder
* Go into project root folder and run the following command in the terminal/command prompt.

```
    npm install
```
* All the dependencies from package.json would be installed in node_modules folder

### **Run Scripts**

* First step is to fire up the selenium server which could be done in many ways, **webdriver-manager** proves very handy for this.The below command should download the **chrome & gecko driver** binaries locally for you!

```javascript
    npm run webdriver-update
```

* Then you should start your selenium server!

```javascript
    npm run webdriver-start
```

* Now just run the test command which launches the Chrome Browser and runs the whole scripts.

```javascript
    npm run test
```

### **HTML Reports**

Currently this project has been integrated with [cucumber-html-reporter](https://www.npmjs.com/package/cucumber-html-reporter), which is generated in the ```reports``` folder when you run ```npm test```. They can be customized according to user's specific needs. Below is the sample test report.

![alt text](images/pro_report.jpg "Report")

### **License**

```   
MIT License

Copyright (c) 2018 Manimaran
```