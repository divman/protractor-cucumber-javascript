import { browser } from "protractor";
import Reporter from "./support/reporter";

const jsonReports = `${process.cwd()}/reports/json`

export const config = {
    // chromeDriver: './drivers/chromedriver',
    // seleniumServerJar: './drivers/selenium-server-standalone-3.12.0.jar',
    seleniumAddress: 'http://localhost:4444/wd/hub',

    // to use the native NodeJs control flow, we need to turn off the Web Driver Control Flow.
    // for this, use the below command to set it to false.
    SELENIUM_PROMISE_MANAGER: false,

    framework: 'custom',

    frameworkPath: require.resolve('protractor-cucumber-framework'),

    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {
            // list of chrome args can be find here https://peter.sh/experiments/chromium-command-line-switches/
            // 'args': ['--disable-extensions=true', 'incognito']
            'args': ['--headless', '--window-size=1280x800']
        }
    },

    // capabilities: {
    //     'browserName': 'firefox',
    //     'moz:firefoxOptions': {
    //         // list of firefox args can be find here https://developer.mozilla.org/en-US/docs/Mozilla/Command_Line_Options
    //         //'args': ['-private']
    //         'args': ['-headless']
    //     }
    // },

    // restartBrowserBetweenTests: true,

    specs: [
        './features/*.feature'
    ],

    cucumberOpts: {
        require: [
            './features/step_definitions/*.js',
            './support/*.js'
        ],
        format: ['json:./reports/json/cucumber_report.json'],
        // format: ['json:./tmp/cucumber_report.json'],
        tags: [
            //"@smoke",
            //"@regression"
        ],
        strict: true
    },

    // another format of html report feature
    // plugins: [{
    //     package: 'protractor-multiple-cucumber-html-reporter-plugin',
    //     options: {
    //         // read the options part for more options
    //         automaticallyGenerateReport: true,
    //         removeExistingJsonReportFile: true,
    //         customData: {
    //             title: 'Manimaran Test Info',
    //             data: [
    //                 {label: 'Project', value: 'Custom project'},
    //                 {label: 'Release', value: '1.2.3'},
    //                 {label: 'Cycle', value: 'B11221.34321'},
    //                 {label: 'Execution Start Time', value: 'Nov 19th 2017, 02:31 PM EST'},
    //                 {label: 'Execution End Time', value: 'Nov 19th 2017, 02:56 PM EST'}
    //             ]
    //         },
    //     pageFooter: '<div><p>A custom footer in html</p></div>'
    //     }
    // }],

    disableChecks: true,

    onPrepare: async () => {
        browser.manage().deleteAllCookies()
        // browser.manage().window().maximize()
        await Reporter.createDirectory(jsonReports)
        // if a web application is non-angular, then make this ignoreSynchronization as true
        // browser.ignoreSynchronization = true;
    },

    getPageTimeout: 100000,
    allScriptsTimeout: 500000,

    onComplete: async () => {
        await Reporter.createHTMLReport()
    }
}
