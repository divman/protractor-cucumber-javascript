import { Before, BeforeAll, After, AfterAll, Status } from "cucumber";
import { config } from "../protractorConf"

import { LoginPageFactory } from "../features/pages/loginPage";
import { WelcomePageFactory } from "../features/pages/welcomePage";
import { VerificationPageFactory } from "../features/pages/verificationPanelPage";
import { HomePageFactory } from "../features/pages/homePage";
import { Reporter } from "./reporter";
import { logger, logFilePath } from "./evenstLogger";
import fs from "fs";

BeforeAll( {timeout: 2 * 20000}, async () => {
    fs.truncateSync(`${logFilePath}/testlogs.log`)
})

Before( {timeout: 2 * 20000}, async (scenario) => {
    // await browser.get(config.baseUrl)

    // All the page object factories are to be registered in the Before Hooks as below,
    // as the framework configuration will open and close the browser for each scenarios under each feature.
    global.loginPage = new LoginPageFactory()
    global.welcomePage = new WelcomePageFactory()
    global.verificationPage = new VerificationPageFactory()
    global.homePage = new HomePageFactory()

    logger.info(`***** Scenario Name : ${scenario.pickle.name} *****`)
    logger.info(`***** Scenario Location :  ${scenario.sourceLocation.uri} : ${scenario.sourceLocation.line} *****`);
})

After( {timeout: 2 * 15000}, async function(scenario) {
    // Taking screenshot of failed step on the webpage.
    if (scenario.result.status === Status.FAILED) {
        const screenshot = await browser.takeScreenshot()
        this.attach(screenshot, "image/png")
    }

    logger.info(`***** Finished Scenario : ${scenario.pickle.name} ***** `)
    logger.info(` `)
    logger.info(`==========================================================`)
    logger.info(` `)

    // To open and close the browser for each scenario, browser restart sould be done after every scenario.
    await browser.restart()
})

AfterAll( {timeout: 2 * 5000}, async () => {
    // after all the protractor tests are done, this will quit the browser completely.
    await browser.quit()
})
