import { configure, getLogger } from "log4js"
import * as path from "path"

export const logFilePath = path.join(process.cwd(), "/logs");

// configuration for the log4js package
configure({
    appenders: {
        normal: {
            type: 'file',
            filename: `${logFilePath}/testlogs.log`,
            maxLogSize: 1048576,
            backups: 2
        }
    },
    categories: {
        default: {
            appenders: ['normal'],
            level: 'debug'
        }
    }
})

export const logger = getLogger('normal')
