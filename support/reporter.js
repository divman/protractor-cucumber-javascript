import * as reporter from "cucumber-html-reporter"
import * as fs from "fs"
import * as mkdirp from "mkdirp";
import * as path from "path"
import os from "os";

const jsonReports = path.join(process.cwd(), "/reports/json");
const htmlReports = path.join(process.cwd(), "/reports/html");
const targetJson = `${jsonReports}/cucumber_report.json`
const htmlReportFile = `${htmlReports}/cucumber_reporter.html`

// options to generate html report for cucumber-html-reporter
const cucumberReportOptions = {
    theme: 'bootstrap',
    jsonFile: targetJson,
    output: htmlReportFile,
    reportSuiteAsScenarios: true,
    metadata: {
        "App Version":"0.3.2",
        "Test Environment": "QA",
        "Browser": "Chrome 54.0.2840.98",
        "Platform": `${os.type()} : ${process.platform}`,
        "Parallel": "Scenarios",
        "Executed": "Local"
    }
}

const Reporter = {
    createDirectory: (dir) => {
        if (!fs.existsSync(dir)) {
            mkdirp.sync(dir) // recursive folder and file creation package
        }
    },

    createHTMLReport: () => {
        try {
            // reporter function to generate a html report from json file
            reporter.generate(cucumberReportOptions)
        } catch (err) {
            if (err) {
                throw new Error(`Failed to save the cucumber test reports to json file. The error is : ${err}`)
            }
        }
    }
}

export default {
    ...Reporter
}

// export class Reporter {

//     static createDirectory(dir) {
//         if (!fs.existsSync(dir)) {
//             // fs.mkdirSync(dir)
//             mkdirp.sync(dir)
//         }
//     }

//     static createHTMLReport() {
//         try {
//             reporter.generate(cucumberReportOptions)
//         } catch (err) {
//             if (err) {
//                 throw new Error(`Failed to save the cucumber test reports to json file. The error is : ${err}`)
//             }
//         }
//     }
// }
